//= require plugins/**/*

//= require plugins/fancybox/jquery.fancybox.js
//= require plugins/fancybox/helpers/jquery.fancybox-media.js
//= require !plugins/fancybox/helpers/jquery.fancybox-buttons.js
//= require !plugins/fancybox/helpers/jquery.fancybox-thumbs.js


//= require scripts/**/*.*
//= require scripts.coffee


$(document).ready(function ($) {

  // init icheck
  (function () {
    var control = $('.radio [type=radio], .checkbox [type=checkbox]');

    control.iCheck();
  }());

  // init main slider
  (function () {
    var slider = $('.js-main-slider');

    slider.bxSlider({
      mode: 'fade',
      controls: false,
      auto: true,
      pause: 8000,
      autoHover: true
    });
  }());

   // collapse
  (function () {
    var el = $('.js-collapse');

    function init() {
      el.on('click', function (event) {
        event.preventDefault();

        var $this = $(this),
          target = $this.data('target');

        if ($this.data('parent') !== undefined && $this.data('parent').length > 0) {
          var parent = $($this.data('parent'));

          el.not($this).removeClass('active');
          $('.collapse', parent).collapse('hide');
        }

        $this.toggleClass('active');
        $(target).collapse('toggle');
      });

      $('.js-close').on('click', function (event) {
        event.preventDefault();

        var parent = $(this).closest('.collapse');

        if (parent.length > 0) {
          parent.collapse('hide');
        }
      });
    }

    if (el.length > 0) {
      return init();
    }
  }());

  // image galery slider
  (function () {
    var thumbs = $('.js-image-thumbs'),
      bigs = $('.js-image-big');

    function init_slider() {
      thumbs.slick({
        dots: false,
        speed: 300,
        slidesToShow: 3,
        variableWidth: true,
        asNavFor: '.js-image-big',
        // centerMode: false,
        focusOnSelect: true
      });

      bigs.slick({
        dots: false,
        arrows: false,
        fade: true
      });
    }

    function init() {
      var img_loaded = imagesLoaded(thumbs);

      img_loaded.on('done', function (instance) {
        init_slider();
      });
    }

    if (thumbs.length > 0) {
      return init();
    }
  }());

  // init crousel
  (function () {
    var slider = $('.js-carousel-goods');

    slider.bxSlider({
      minSlides: 3,
      maxSlides: 3,
      speed: 200,
      slideWidth: 220,
      slideMargin: 2,
      moveSlides: 1,
      pager: false
    });
  }());

  // init range slider
  (function () {
    var $el = $('.js-widget-range');

    if ($el.length > 0) {

      $el.each(function () {
        var $this = $(this),
          slider = $('.js-range-slider', $this),
          min = $('.input-min', $this),
          max = $('.input-max', $this);

        slider.nstSlider({
          left_grip_selector: ".grip-left",
          right_grip_selector: ".grip-right",
          value_bar_selector: ".bar",
          value_changed_callback: function (cause, leftValue, rightValue) {
            if (min.length > 0) {
              min.val(leftValue);
            }
            if (max.length > 0) {
              max.val(rightValue);
            }
          }
        });

        min.add(max).on('change', function () {
          slider.nstSlider('set_position', min.val(), max.val());
        });
      });
    }
  }());

  // js-summ' - вспомогательная функция для подсчета суммы, используется в корзине
  function calc_summ(el) {
    var parent = el.closest('.js-calc'),
      price = parseFloat($('.js-price', parent).text()),
      num = parseFloat($('.js-quantity-field', parent).val());

    $('.js-summ', parent).html(price * num);

    var total = 0;

    $('.js-summ').each(function () {
      return total += parseFloat($(this).html());
    });

    $('.js-cart-total .summ').html(total);
  }

  // js-quantity
  (function () {
    var el = $('.js-quantity');

    function init() {
      el.each(function () {
        var $this = $(this),
          input = $('.js-quantity-field', $this),
          max = $('.max', $this),
          min = $('.min', $this),
          c_max = parseFloat(input.prop('max')),
          c_min = parseFloat(input.prop('min'));

        max.on('click', function (event) {
          event.preventDefault();

          var val = parseFloat(input.val());

          if (val >= c_max) {
            console.log('The value can not be greater than');
          } else {
            input.val(val + 1);

            if ($this.closest('.js-calc').length > 0) {
              calc_summ($this);
            }
          }
        });

        min.on('click', function (event) {
          event.preventDefault();

          var val = parseFloat(input.val());

          if (val <= c_min) {
            console.log('The number can not be negative');
          } else {
            input.val(val - 1);

            if ($this.closest('.js-calc').length > 0) {
              calc_summ($this);
            }
          }
        });
      });
    }

    if (el.length > 0) {
      return init();
    }
  }());

  (function () {
    var parent = $('.catalog-links');

    function init() {
      var sub = $('.catalog-links > .item > .sub'),
        // item = sub.closest('.item'),
        link = sub.siblings('.link');

      link.on('click', function (event) {
        event.preventDefault();

        var $this = $(this);

        sub.hide();
        $this
          .siblings('.sub')
          .slideDown();
      });
    }

    if (parent.length > 0) {
      return init();
    }
  }());
});