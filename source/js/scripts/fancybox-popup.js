$(document).ready(function($) {

  // easy photo and video gallery
  $('.js-gallery-fancybox').fancybox({
    padding: 15,
    nextEffect: 'fade',
    prevEffect: 'fade',
    wrapCSS: 'fancybox-gallery-skin',
    helpers: {
      media: true, // for video gallery
      title : {
        type : 'inside'
      },
      overlay : {
        locked : false
      }
    }
  });

  // simple popups
  (function () {
    var el = $('.js-popup');

    function init() {
      el.on('click', function (event) {
        event.preventDefault();

        var $this = $(this),
          selector,
          target;

        if ($this.data('target') !== undefined && $this.data('target') !== false) {
          selector = $this.data('target');
        } else {
          selector = $this.attr('href');
        }

        target = $(selector);

        $.fancybox.open(target, {
          padding: 0,
          margin: 0,
          autoResize: true,
          wrapCSS: "fancybox-popup",
          helpers : {
            overlay : {
              locked : false
            }
          }
        });

      });
    }

    if (el.length > 0) {
      return init();
    }
  }());

});